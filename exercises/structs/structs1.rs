// structs1.rs
// Address all the TODOs to make the tests pass!

use std::borrow::Cow;
struct ColorClassicStruct<'a> {
    name: Cow<'a, str>,
    hex: Cow<'a, str>,
}

struct ColorTupleStruct<'a>(Cow<'a, str>,Cow<'a, str>);

#[derive(Debug)]
struct UnitStruct;

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn classic_c_structs() {
        // TODO: Instantiate a classic c struct!
        let green = ColorClassicStruct {
            name: Cow::from("green"),
            hex: Cow::from("#00FF00")
        };

        assert_eq!(green.name, "green");
        assert_eq!(green.hex, "#00FF00");
    }

    #[test]
    fn tuple_structs() {
        // TODO: Instantiate a tuple struct!
        let green = ColorTupleStruct(Cow::from("green"),Cow::from("#00FF00"));

        assert_eq!(green.0, "green");
        assert_eq!(green.1, "#00FF00");
    }

    #[test]
    fn unit_structs() {
        // TODO: Instantiate a unit struct!
        let unit_struct = UnitStruct {};
        let message = format!("{:?}s are fun!", unit_struct);

        assert_eq!(message, "UnitStructs are fun!");
    }
}
